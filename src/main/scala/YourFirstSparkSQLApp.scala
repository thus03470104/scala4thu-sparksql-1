import org.apache.spark.SparkConf
import org.apache.spark.sql.{DataFrame, SparkSession}

/**
  * Created by mark on 04/06/2017.
  */
object YourFirstSparkSQLApp extends App{

  val ss = SparkSession
    .builder()
    .master("local[*]")
    .appName("Spark SQL first example")
    .getOrCreate()
  val src: DataFrame =ss.read.option("header", "true")
    .csv("dataset/small-ratings.csv")
  src.createOrReplaceTempView("ratings")
  ss.sql("SELECT * FROM ratings WHERE movieId=30707 AND userId=107799").show()
  val moviesrc: DataFrame =ss.read.option("header", "true")
    .csv("dataset/movies.csv")
  src.createOrReplaceTempView("movies")
  ss.sql("SELECT movieId,title FROM movies WHERE movieId=10").show()
}
//SELECT movieId,max(rating),min(rating) FROM ratings GROUP BY movieId;
//-- 每部電影評價的最大最小值（可能是期末考）
//SELECT movieId,avg(rating) as mean FROM ratings GROUP BY movieId ORDER BY avg(rating) DESC ;
//-- 第三題 試著從small-ratings.csv中，計算出平均rating並根據平均rating做降序排列，並show與以下欄位相同的資料格式
//SELECT * FROM ratings WHERE movieId=30707 AND userId=107799;
//-- 第四題 試著從small-ratings.csv中，找出movieId為30707且userId=107799，並show與以下欄位相同的資料格式
//SELECT movieId,title FROM movies WHERE movieId=10;
//-- 第五題 試著從movie.csv中，找出movieId為10的資料, 並show與以欄位相同的資料